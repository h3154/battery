# Battery reader

A CLI battery reader program for the PinePhone.

**Foreword:** I'm a PHP developer, not a C++ developer.
This code was written as a practice, so don't be too surprised if you find some weird bits in the code.

***Reviews (and merge requests if you want) are welcome!***

## History

When I first got my PinePhone, my first thing to do on it was to write a Bash script that reads the battery of the
phone, and prints out an ASCII battery with the info next to it. I used this script through SSH. Then I got myself the
PinePhone keyboard, which also has a battery in it, which was (naturally) not picked up by the old Bash script, so I
needed a new version that can read and display all batteries (that I can think of). This is that new program.

## Compiling

There are no external dependencies as of yet, so compiling it should be pretty straightforward.

```bash
# Create a folder where you can compile it, then enter it:
mkdir build && cd build

# Prepare the cmake script with one of the lines:
# Production mode (-O3)
cmake ..

# Debug mode (-g)
cmake -DCMAKE_BUILD_TYPE=DEBUG ..

# TODO:
# Check how to define a macro when compiling, so the battery info path can be handled better
# For now the macro value can be edited manually

# Compile it
make -j
# You can also use the `-j` flag to enable the usage of multiple CPU cores
# It also accepts a number parameter if you don't want to use all the cores
```

## Usage

As there are no flags or anything, just call it and watch the magic:
```bash
./battery
```

## A few classes

### Battery

A simple battery class that can store data of the battery such as capacity, voltage, etc.
It can also draw itself as an ASCII battery.

### Reader

A class that can read in a battery's status and return a new battery instance

### Utilities::Table, Utilities::Column

Can be used to write data next to each other, as opposed to under each other.
Supports only a single row atm, and it's kind of a mess with all the wstring-string and escape character filtering magic.

### Units::Unit

Tiny class that helps making units human readable. Specific units are implemented in child classes.
