#include <iostream>
#include "battery.hpp"
#include "reader.hpp"
#include "utilities/batterypack.hpp"

// Macros used in this file get defined here!
#include "main.hpp"

int main() {
    #if ART_TEST_MODE
        batteryTest();
        return 0;
    #endif

    Utilities::BatteryPack pack;

    std::vector<std::filesystem::path> paths;
    const std::filesystem::path dir(BATTERY_DIR);
    std::size_t batteryCount = 0;
    for (auto const& entry : std::filesystem::directory_iterator(dir)) {
        batteryCount++;
    }
    paths.reserve(batteryCount);
    pack.reserve(batteryCount);
    for (auto const& entry : std::filesystem::directory_iterator(dir)) {
        paths.push_back(entry.path());
    }
    std::sort(paths.begin(), paths.end());

    for (const auto path : paths) {
        try {
            Battery *battery = Reader::read(path);
            // TODO: Make switch handling, and add not-present battery reporting optional
            if (battery && battery->present()) {
                pack.emplace_back(battery);
            }
        } catch (std::ios_base::failure error) {
            std::cerr << error.what() << "\n";
        }
    }

    std::cout << pack << "\n";

    return 0;
}

#if ART_TEST_MODE
    void batteryTest() {
        for (int row = 0; row < 10; row++) {
            Utilities::BatteryPack pack;
            pack.reserve(10);
            pack.debug = true;

            std::array<Battery*, 10> batteries;
            for (int i = 0; i < 10; i++) {
                batteries[i] = new Battery("Dummy");
                batteries[i]
                    ->capacity(row * 10 + i)
                    ->present(true)
                    ->charging(false);
                pack.emplace_back(batteries[i]);
            }

            std::cout << pack << " " << row * 10 << "-" << (row + 1) * 10 - 1 << "%\n";
        }
    }
#endif
