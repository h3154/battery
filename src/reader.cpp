#include "reader.hpp"
#include <fstream>

Reader::Reader(const std::filesystem::path &dataRoot) : dataRoot(dataRoot) {
    // ...
}

bool Reader::rootExists() const {
    std::filesystem::directory_entry root(dataRoot);
    return root.exists();
}

std::string Reader::readFile(const std::filesystem::path& path) const {
    std::string str;
    std::filesystem::path filepath = dataRoot / path;
    std::ifstream in(filepath);
    if (in.fail()) {
        throw std::ios_base::failure((std::string)"Failed to open file: " + filepath.string());
    }
    std::getline(in, str);
    return str;
}

void Reader::readBattery() {
    battery = new Battery(dataRoot.filename());

    bool present;
    try {
        present = std::stold(readFile("present"));
    } catch (std::ios_base::failure) {
        // Megi said if present doesn't exist, it's always present
        present = true;
    }
    battery->present(present);

    // If battery is not present, don't even try reading anything else, it will contain only error and garbage!
    if (!present) {
        return;
    }

    // TODO: handle capacity missing like for the keyboard
    try {
    battery
        ->capacity(std::stold(readFile("capacity")))
        ->current(std::stold(readFile("current_now")) * Units::Unit::micro)
        ->voltage(std::stold(readFile("voltage_now")) * Units::Unit::micro)
        ->charging(readFile("status") == "Charging")
        ->health(readFile("health"));
    } catch (...) {
        delete battery;
        std::rethrow_exception(std::current_exception());
    }
}

Battery *Reader::read(const std::filesystem::path &dataRoot) {
    Reader reader(dataRoot);

    // If battery path doesn't exist (not connected) we do an early return
    if (!reader.rootExists()) {
        return reader.battery;
    }
    reader.readBattery();

    return reader.battery;
}
