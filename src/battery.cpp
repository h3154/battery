#include "battery.hpp"
#include "utilities/stringhelper.hpp"

//////////////////
// Constructors //
//////////////////
Battery::Battery(const std::string &name) : _name(name) {}

////////////////////////
// Colour calculators //
////////////////////////
inline void Battery::calculateInnerColour() {
    if (_capacity >= 50) {
        innerColour = "\e[32m";
    } else if (_capacity >= 25) {
        innerColour = "\e[33m";
    } else {
        innerColour = "\e[31m";
    }
}

inline void Battery::calculateOuterColour() {
    if (!_present) {
        outerColour = "\e[90m";
    } else if (_charging) {
        outerColour = "\e[96m";
    } else {
        outerColour = "\e[0m";
    }
}

/////////////
// Getters //
/////////////
const std::string &Battery::name() const {
    return _name;
}

int Battery::capacity() const {
    return _capacity;
}

const Units::Ampere &Battery::current() const {
    return _current;
}

const Units::Volt &Battery::voltage() const {
    return _voltage;
}

bool Battery::charging() const {
    return _charging;
}

bool Battery::present() const {
    return _present;
}

const std::string &Battery::health() const {
    return _health;
}

/////////////
// Setters //
/////////////
Battery *Battery::capacity(int capacity) {
    _capacity = capacity;
    calculateInnerColour();
    return this;
}

Battery *Battery::current(const Units::Ampere &current) {
    _current = current;
    return this;
}

Battery *Battery::voltage(const Units::Volt &voltage) {
    _voltage = voltage;
    return this;
}

Battery *Battery::charging(bool charging) {
    _charging = charging;
    calculateOuterColour();
    return this;
}

Battery *Battery::present(bool present) {
    _present = present;
    calculateOuterColour();
    return this;
}

Battery *Battery::health(const std::string &health) {
    _health = health;
    return this;
}

////////////
// Pieces //
////////////
const std::string Battery::Parts::top =    " ╔════╗";
const std::string Battery::Parts::neck =   "╔╝    ╚╗";
const std::string Battery::Parts::middle = "║      ║";
const std::string Battery::Parts::bottom = "╚══════╝";

/////////////
// Drawers //
/////////////
std::string Battery::getConnectorPiece() const {
    return outerColour + "═\e[0m";
}

std::string Battery::drawLine(std::string str, const unsigned int &level) const {
    std::regex spaceRegex(" ");
    std::regex colourRegex("( ){2,}");
    std::regex frameRegex("((╔)|(═)|(╗)|(╚)|(╝)|(║))+");
    if (capacity() >= (level * 10)) {
        std::string blocks[] = {" ", "░", "▒", "▓", "█"};
        std::string block = blocks[4];
        if (capacity() / 10 == level) {
            block = blocks[capacity() % 10 / (10 / 5)];
        }
        str = std::regex_replace(str, colourRegex, innerColour + "$0\e[0m");
        str = std::regex_replace(str, spaceRegex, block);
    }
    str = std::regex_replace(str, frameRegex, outerColour + "$0\e[0m");
    return str;
}

std::string Battery::draw(bool first, const Battery* next) const {
    // If this is **not** the first battery we draw,
    // we have to cut off the first few characters
    // Using a local macro (undefined before the end),
    // so it's no longer an unnecessary lambda anymore if the first battery
    #define CUT_IF_NOT_FIRST(str) (first ? str : Utilities::StringHelper::withoutFirst(str, 3))

    std::stringstream out;

    // Print the top and the connector first
    unsigned int level = 10;
    out << drawLine(CUT_IF_NOT_FIRST(Parts::top), level--) << (next ? next->getConnectorPiece() : " ") << "\n"
        << drawLine(CUT_IF_NOT_FIRST(Parts::neck), level--) << "\n";

    // Draw the middle bit
    for (int i = 9; i > 0; i--) {
        out << drawLine(CUT_IF_NOT_FIRST(Parts::middle), level--) << "\n";
    }

    // Draw the bottom
    out << drawLine(CUT_IF_NOT_FIRST(Parts::bottom), 0);

    return out.str();

    #undef CUT_IF_NOT_FIRST
}

std::ostream &operator<<(std::ostream &out, const Battery *battery) {
    out << battery->name() << ":" << "\n";
    if (battery->present()) {
        out << "    Capacity: " << battery->capacity() << "%\n"
            << "    Current: " << battery->current() << "\n"
            << "    Voltage: " << battery->voltage() << "\n"
            << "    Charging: " << (battery->charging() ? "Charging" : "Discharging") << "\n"
            << "        with: " << (battery->current() * battery->voltage()) << "\n"
            << "    Health: " << battery->health();
    } else {
        out << "    Not present";
    }
    return out;
}
