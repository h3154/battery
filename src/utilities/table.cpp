#include "utilities/table.hpp"
#include "utilities/stringhelper.hpp"

#include <algorithm>
#include <regex>

namespace Utilities {
    Table::Column::Column() {
        // So there is always a line at the start!
        lines.push_back(std::make_shared<std::stringstream>());
    }

    std::shared_ptr<Table::Column> Table::getColumn() {
        auto column = std::make_shared<Column>();
        columns.push_back(column);
        return column;
    }

    Table::Column &Table::Column::setDivider(const std::string &divider) {
        this->divider = divider;
        return *this;
    }

    const std::stringstream Table::Column::empty;

    const std::stringstream *Table::Column::getLine(unsigned int line) const {
        try {
            return lines.at(line).get();
        } catch (std::out_of_range&) {
            return &empty;
        }
    }

    std::string::size_type Table::Column::stringLengthWithoutEscapeCharacters(const std::string &str) {
        std::regex regex("(\e\\[[^m]+m)");
        std::string string = std::regex_replace(str, regex, "");
        return Utilities::StringHelper::stringToWstring(string).length();
    }

    std::shared_ptr<Table::Column> &operator<<(std::shared_ptr<Table::Column> &column, const std::string &str) {
        // Convert to wstring, so substr() doesn't cut unicode in half
        std::wstring ws = Utilities::StringHelper::stringToWstring(str);

        // Terminal case of recursion: contains no newlines
        auto pos = ws.find(L"\n");
        if (pos == std::wstring::npos) {
            // Contains no newline, so we can just use the original
            *(column->lines.back()) << str;
            column->longestLine = std::max(
                column->longestLine,
                Table::Column::stringLengthWithoutEscapeCharacters(column->lines.back()->str())
            );
            return column;
        }

        // If contains new lines, recursion happens
        // Calls itself twice, but the first one is **guaranteed** not to recurse
        column << Utilities::StringHelper::wstringToString(ws.substr(0, pos));

        // Have to check if there's anything after the newline
        if (pos != str.size()) {
            column->lines.push_back(std::make_shared<std::stringstream>());
            column << Utilities::StringHelper::wstringToString(ws.substr(pos + 1));
        }

        return column;
    }

    std::ostream &operator<<(std::ostream &out, const Table &table) {
        // Check the tallest column, to see how many lines we have to go through
        std::vector<unsigned int> columnLines;
        unsigned int tallestColumnLength = 0;
        for (const auto column : table.columns) {
            columnLines.push_back(column->lines.size());
        }

        // C++, why??
        tallestColumnLength = columnLines.at(
            std::distance(columnLines.begin(), std::max_element(columnLines.begin(), columnLines.end()))
        );

        // Print each line of each column
        for (unsigned int i = 0; i < tallestColumnLength; i++) {
            for (const auto column : table.columns) {
                out << column->divider;
                std::string line = column->getLine(i)->str();
                out << line;
                out << std::string(column->longestLine - Table::Column::stringLengthWithoutEscapeCharacters(line), ' ');
            }
            // Don't add extra newline at the end, let the user do that
            if (i != tallestColumnLength - 1) {
                out << "\n";
            }
        }

        return out;
    }
};
