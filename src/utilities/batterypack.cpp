#include "utilities/batterypack.hpp"
#include "utilities/stringhelper.hpp"
#include "utilities/table.hpp"

std::ostream &operator<<(std::ostream &out, const Utilities::BatteryPack &pack) {
    using Utilities::StringHelper;

    Utilities::Table table;

    for (unsigned int i = 0; i < pack.size(); i++) {
        bool first = !i;
        Battery *next;
        try {
            next = pack.at(i + 1).get();
        } catch (std::out_of_range&) {
            next = NULL;
        }

        auto column = table.getColumn();
        column->setDivider("");
        column << pack.at(i)->draw(first, next);
    }

    if (pack.debug) {
        out << table;
        return out;
    }

    for (int i = 0; i < pack.size(); i++) {
        auto col = table.getColumn();
        col->setDivider("");
        bool last = i == pack.size() - 1;
        col << pack.at(i).get();
        // We add newlines only if this is the last battery, so column won't contain empty lines
        if (!last) {
            col << "\n\n";
        }
    }

    out << table;

    return out;
}
