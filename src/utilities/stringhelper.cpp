#include "utilities/stringhelper.hpp"

namespace Utilities {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> StringHelper::converter;

    std::wstring StringHelper::stringToWstring(const std::string &str) {
        return StringHelper::converter.from_bytes(str);
    }

    std::string StringHelper::wstringToString(const std::wstring &str) {
        return StringHelper::converter.to_bytes(str);
    }

    std::string StringHelper::withoutFirst(const std::string &str, const unsigned int &i) {
        return wstringToString(stringToWstring(str).substr(i));
    }
};
