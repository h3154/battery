#include "units/volt.hpp"

namespace Units {
    std::string Volt::getSymbol() const {
        return "V";
    }

    std::string Volt::getName() const {
        return "volt";
    }
};
