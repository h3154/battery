#include "units/unit.hpp"

#include <iostream>

namespace Units {
    const long double Unit::micro = std::pow(10, -6);

    std::map<int, Unit::PrefixName> Unit::prefixes = {
        {-4, {-4, " p", " piko"}},
        {-3, {-3, " n", " nano"}},
        {-2, {-2, " µ", " mikro"}},
        {-1, {-1, " m", " mili"}},
        {0, {0, " ", " "}},
        {1, {1, " k", " Kilo"}},
        {2, {2, " M", " Mega"}},
        {3, {3, " G", " Giga"}},
        {4, {4, " T", " Tera"}},
        {5, {5, " P", " Peta"}},
        {6, {6, " E", " Exa"}},
        {7, {7, " Z", " Zetta"}},
        {8, {8, " Y", " Yotta"}},
    };

    Unit::Unit(const long double &value) : value(value) {
        // ...
    }

    long double Unit::getRawValue() const {
        return value;
    }

    std::ostream &operator<<(std::ostream &out, const Unit &unit) {
        auto prefix = unit.getPrefix();
        auto precision = out.precision(3); // We store the precision, so we can restore it afterwards
        out << unit.getRawValue() / (std::pow(1000, prefix.powThousand))
            << prefix.symbol // (longname ? prefix.name : prefix.symbol)
            << unit.getSymbol() // (longname ? getName() : getSymbol());
        ;
        out.precision(precision);
        return out;
    };

    Unit::PrefixName Unit::getPrefix() const {
        int powThousand = 0;
        if (value != 0) {
            powThousand = std::floor(std::log(value) / std::log(1000)); // log(x) / log(y) is the log(x) in base y
        }

        if (Unit::prefixes.contains(powThousand)) {
            return Unit::prefixes.at(powThousand);
        }

        return Unit::prefixes.at(0);
    }
};
