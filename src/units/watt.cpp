#include "units/watt.hpp"

namespace Units {
    std::string Watt::getSymbol() const {
        return "W";
    }

    std::string Watt::getName() const {
        return "watt";
    }

    Watt operator*(const Ampere &a, const Volt &v) {
        return a.getRawValue() * v.getRawValue();
    }

    Watt operator*(const Volt &v, const Ampere &a) {
        return a * v;
    }
};
