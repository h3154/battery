#include "units/ampere.hpp"

namespace Units {
    std::string Ampere::getSymbol() const {
        return "A";
    }

    std::string Ampere::getName() const {
        return "ampere";
    }
};
