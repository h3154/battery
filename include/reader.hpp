#pragma once

#include <filesystem>
#include <string>
#include "battery.hpp"

class Reader {
 protected:
    Battery *battery = NULL;
    std::filesystem::path dataRoot;

    Reader(const std::filesystem::path &dataRoot);
    bool rootExists() const;

    void readBattery();
    std::string readFile(const std::filesystem::path &filePath) const;

 public:
    /**
     * Reads the details for a battery and returns a pointer to it
     *
     * If battery does not exist, we return null, otherwise a valid pointer
     *
     * @param  batteryName The name for the battery constructor
     * @param  dataPath    The path we try to read the data from
     */
    static Battery *read(const std::filesystem::path &dataRoot);
};
