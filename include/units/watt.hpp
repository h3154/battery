#pragma once

#include "units/unit.hpp"
#include "units/ampere.hpp"
#include "units/volt.hpp"

namespace Units {
    class Watt : public Unit {
     protected:
        std::string getSymbol() const override;
        std::string getName() const override;

     public:
        using Unit::Unit;
    };

    Watt operator*(const Volt&, const Ampere&);
    Watt operator*(const Ampere&, const Volt&);
};
