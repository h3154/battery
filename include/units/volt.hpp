#pragma once

#include "units/unit.hpp"

namespace Units {
    class Volt : public Unit {
     protected:
        std::string getSymbol() const override;
        std::string getName() const override;
     public:
        using Unit::Unit;
    };
};
