#pragma once

#include <cmath>
#include <map>
#include <sstream>
#include <string>
#include <tuple>

namespace Units {
    /**
     * A unit class, so I can have pretty human-friendly numbers on the screen
     */
    class Unit {
     protected:
        /** Prefixes have short (usually one letter) and long names */
        struct PrefixName {
            /** SI units have new names every 1000^3n */
            int powThousand;

            /** The one letter symbol and the proper name of the unit */
            std::string symbol, name;
        };

        /**
         * What prefixes the unit gains
         *
         * K => V map
         * 1000^(3K) gains means V prefix
         */
        static std::map<int, PrefixName> prefixes;

        /** Using the value of the unit, returns with a PrefixName */
        PrefixName getPrefix() const;

        /** The real value of the unit */
        long double value;

        /**
         * Return the symbol of the unit
         *
         * Override this in child classes
         */
        virtual std::string getSymbol() const = 0;

        /**
         * Return the name of the unit
         *
         * Override this in child classes
         */
        virtual std::string getName() const = 0;

     public:
        /** No default constructor, give a value */
        Unit() = delete;
        Unit(const long double&);

        /** Getter for the **raw** value as double */
        long double getRawValue() const;

        /** Convert the unit to a human readable format */
        friend std::ostream &operator<<(std::ostream&, const Unit&);

        const static long double micro;
    };
};


