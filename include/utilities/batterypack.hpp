#pragma once

#include <memory>
#include <vector>
#include "battery.hpp"

namespace Utilities {
    class BatteryPack : public std::vector<std::unique_ptr<Battery>> {
     public:
        bool debug = false;
    };
};

std::ostream &operator<<(std::ostream&, const Utilities::BatteryPack&);
