#pragma once

#include <codecvt>
#include <locale>

namespace Utilities {
    class StringHelper {
     protected:
        static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;

     public:
        static std::wstring stringToWstring(const std::string&);
        static std::string wstringToString(const std::wstring&);
        static std::string withoutFirst(const std::string&, const unsigned int&);
    };
};
