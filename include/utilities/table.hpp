#pragma once

#include <iostream>
// #include <list>
#include <memory>
#include <string>
#include <vector>

namespace Utilities {
    class Table {
     public:
        class Column {
         protected:
            /** What to put on the LEFT side */
            std::string divider = " ";

            /** The lines within this column */
            // TODO: Replace with std::list, once I figure out how to rewrite `getLine()`
            std::vector<std::shared_ptr<std::stringstream>> lines = {};

            // Placeholder
            const static std::stringstream empty;

            /** The longest line within the column */
            std::string::size_type longestLine = 0;

         public:
            /** Constructor */
            Column();

            /** Setter for the divider */
            Column &setDivider(const std::string&);

            /** Get a specific line, or an empty string if it doesn't exists */
            const std::stringstream *getLine(unsigned int) const;

            /** Returns the length of a string, ignoring the escape characters */
            static std::string::size_type stringLengthWithoutEscapeCharacters(const std::string&);

            /**
             * Adds a new line to the lines
             *
             * Checks for `\n` characters, so they're not required
             *
             * Since they MUST be implemented as non-members, they have to be marked as friend
             */
            friend std::shared_ptr<Column> &operator<<(std::shared_ptr<Column>&, const std::string&);

            /** The table printer is marked as friend too */
            friend std::ostream &operator<<(std::ostream&, const Table&);
        };

     protected:
        /** The columns within the table */
        std::vector<std::shared_ptr<Column>> columns;

     public:
        /** Default constructor is fine */
        Table() = default;

        /** Creates a new column and returns it */
        std::shared_ptr<Column> getColumn();

        /** Print the table to ostream */
        friend std::ostream &operator<<(std::ostream&, const Table&);
    };

    // Template specification
    std::shared_ptr<Table::Column> &operator<<(std::shared_ptr<Table::Column> &column, const std::string&);

    template <typename T>
    std::shared_ptr<Table::Column> &operator<<(std::shared_ptr<Table::Column> &column, const T &obj) {
        std::stringstream str;
        str << obj;
        column << str.str();
        return column;
    }
};
