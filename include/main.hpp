// Was torn how to name this file, main.hpp or config.hpp
// Went with main.hpp, because this is the only file we pull it in
// and the macros defined in here are used only in main.cpp

// Where do we want to read the battery data?
#ifndef BATTERY_DIR
#define BATTERY_DIR "/sys/class/power_supply/"
#endif

// Do we want to just test how the batteries draw 0-99%?
#ifndef ART_TEST_MODE
#define ART_TEST_MODE false
#endif

#if ART_TEST_MODE
    void batteryTest();
#endif
