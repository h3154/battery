#pragma once

#include "units/ampere.hpp"
#include "units/volt.hpp"
#include "units/watt.hpp"
#include <string>

class Battery {
 protected:
    /** Displayed name of the battery */
    std::string _name;

    // The details of the battery
    // They're prefixed with an underscore, so I can name the getters and setters the same
    int _capacity = 0;
    Units::Ampere _current = 0;
    Units::Volt _voltage = 0;
    bool _charging = false;
    bool _present = false;
    /** https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power#:~:text=/sys/class/power_supply/%3Csupply_name%3E/health */
    std::string _health;

    /** Parts for drawing the battery */
    struct Parts {
        static const std::string top,
            neck,
            middle,
            bottom;
    };

    /** The tiny bit we want to draw on the top right corner for the previous battery */
    std::string getConnectorPiece() const;

    /** Draws a line of the battery, filling in the empty space depending on the charge status */
    std::string drawLine(std::string, const unsigned int&) const;

    /**
     * Terminal colouring codes for escape sequences
     *
     * Default is black (empty) internals with white frame
     */
    std::string innerColour = "\e[30m", outerColour = "\e[0m";

    inline void calculateInnerColour();
    inline void calculateOuterColour();

 public:
    Battery() = delete;
    Battery(const std::string &name);

    // Getters
    const std::string &name() const;
    int capacity() const;
    const Units::Ampere &current() const;
    const Units::Volt &voltage() const;
    bool charging() const;
    bool present() const;
    const std::string &health() const;

    // Chainable setters returning `this`
    Battery *capacity(int capacity);
    Battery *current(const Units::Ampere &current);
    Battery *voltage(const Units::Volt &voltage);
    Battery *charging(bool charging);
    Battery *present(bool present);
    Battery *health(const std::string &health);

    /**
     * Method that draws the battery as an ASCII drawing
     *
     * @param  first Whether this battery is the first one to be drawn
     *               (if not the first, the first few characters are cut off)
     * @param  next  The pointer to the next battery if there's any more after this
     *               Used for drawing the connector piece
     */
    std::string draw(bool first, const Battery* next = NULL) const;
};

std::ostream &operator<<(std::ostream&, const Battery*);
